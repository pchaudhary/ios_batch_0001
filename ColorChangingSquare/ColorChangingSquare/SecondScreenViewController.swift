//
//  SecondScreenViewController.swift
//  ColorChangingSquare
//
//  Created by Cerebro on 17/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class SecondScreenViewController: UIViewController {
    @IBOutlet weak var square: UIView!
    var isRed = true

    override func viewDidLoad() {
        super.viewDidLoad()

        if isRed {
            square.backgroundColor = UIColor.redColor()
        } else {
            square.backgroundColor = UIColor.greenColor()
        }
        
        
        
    var vc1 = ViewController()
        var vc2 = ViewController()
        var vc3 = ViewController()
        var vc4 = ViewController()
        
        
        vc1.showSquareScreen(true)
        vc2.showSquareScreen(true)
        vc3.showSquareScreen(true)
        vc4.showSquareScreen(true)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
