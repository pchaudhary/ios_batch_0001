//
//  CKCustomTableViewCell.swift
//  CustomViews
//
//  Created by Cerebro on 15/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class CKCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet private weak var vegNonVegImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    override func awakeFromNib() {
//        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func isVeg(isVeg:Bool) {
        if(isVeg) {
            vegNonVegImageView.image = UIImage(named: "lion.jpg");
        } else {
            vegNonVegImageView.image = UIImage(named: "shark.jpeg");
        }
    }
    
}
