//
//  Project.swift
//  CKApiClass
//
//  Created by Cerebro on 25/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import Foundation
import SwiftyJSON

class Project {
    var identifier:Int?
    var title:String
    var subdomain:String?
    var role:Role?
    
    init(title:String) {
        self.title = title
        self.identifier = nil
        self.subdomain = nil
        self.role = nil
    }
    
    init(title:String, subdomain:String) {
        self.title = title
        self.identifier = nil
        self.subdomain = subdomain
        self.role = nil
    }
    
    init(json:JSON) {
        self.title = ""
        self.identifier = nil
        self.subdomain = nil
        self.role = nil
        
        if let title = json["title"].string {
            self.title = title
        }
        
        if let role = json["role"].string {
            //convert string to enum
            self.role = .OWNER
        }
        
        if let subdomain = json["subdomain"].string {
            self.subdomain = subdomain
        }
        
        if let identifier = json["id"].int {
            self.identifier = identifier
        }
    }
}

enum Role{
    case OWNER, ADMIN, READ, WRITE
}
