//
//  ViewController.swift
//  CollectionViewDemo
//
//  Created by Cerebro on 30/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        fetchData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("design1", forIndexPath: indexPath) as! UICollectionViewCell
        
        var image = UIImage(named: "mvc.png")
        var imageView = UIImageView(image: image)
        
        
        var image2 = UIImage(named: "logo.png")
        var imageView2 = UIImageView(image: image2)
        
        cell.backgroundView = imageView
        cell.selectedBackgroundView = imageView2
        
        var labelFrame = CGRect(x: 0, y: 50, width: 50, height: 30)
        var label = UILabel(frame: labelFrame)
        label.text = "Hamara Bajaj"
        
        var buttonFrame = CGRect(x: 0, y: 0, width: 80, height: 40)
        var button = UIButton(frame: buttonFrame)
        button.setTitle("Hello", forState: .Normal)
        
        cell.contentView.addSubview(label)
        cell.contentView.addSubview(button)
        
        return cell
    }
    
    func fetchData() {
        
    }

}

