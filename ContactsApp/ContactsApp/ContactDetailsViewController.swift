//
//  ContactDetailsViewController.swift
//  ContactsApp
//
//  Created by Cerebro on 12/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ContactDetailsViewController: UIViewController {
    
    var userId:Int?
    var contact:CKContact?
    
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println("User id that I got is \(userId)")
        
        let contacts = CKContactsService().contacts()
        
        if userId != nil {
            contact = contacts[userId!]
            emailLabel.text = contact!.email
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
