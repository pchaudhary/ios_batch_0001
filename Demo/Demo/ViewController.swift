//
//  ViewController.swift
//  Demo
//
//  Created by Cerebro on 11/09/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var animator:UIDynamicAnimator?
    var gravity = UIGravityBehavior()
    var collision = UICollisionBehavior()
    var itemBehaviour = UIDynamicItemBehavior()
    
    var brickWidth = 40
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animator = UIDynamicAnimator(referenceView: self.view)
        animator?.addBehavior(gravity)
        animator?.addBehavior(collision)
        collision.translatesReferenceBoundsIntoBoundary = true
        animator?.addBehavior(itemBehaviour)
        
        itemBehaviour.elasticity = 1.0
        itemBehaviour.friction = 0.0
        itemBehaviour.resistance = CGFloat(0.0)
    }
    
    @IBAction func screenTapped(sender: UITapGestureRecognizer) {
        var brickCount = Int(self.view.bounds.size.width) / brickWidth
        
        var aVar = Int(arc4random_uniform(UInt32(brickCount)))
        
        var frame = CGRect(x: aVar*brickWidth, y: 0, width: brickWidth, height: brickWidth)
        
        var brick = UIView(frame: frame)
        
        brick.backgroundColor = self.randomColor()
        
        
        
        self.view.addSubview(brick)
        gravity.addItem(brick)
        collision.addItem(brick)
        itemBehaviour.addItem(brick)
        
        
    }
    
    public func randomColor() -> UIColor {
        var aVar = Int(arc4random_uniform(UInt32(5)))
        
        switch (aVar) {
        case 0:
            return UIColor.greenColor()
        case 1: return UIColor.blackColor()
        case 2: return UIColor.redColor()
        case 3: return UIColor.blueColor()
        default: return UIColor.yellowColor()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

