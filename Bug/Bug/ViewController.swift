//
//  ViewController.swift
//  Bug
//
//  Created by Cerebro on 23/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.dataSource = self
        
        Alamofire.request(.GET, "http://api.invidz.com/users", parameters: nil).responseJSON { (request, response, data, error) in
            if(error != nil) {
                NSLog("Error: \(error)")
                println(request)
                println(response)
            }
            else {
                var json = JSON(data!)
                self.users = User.usersFromJSON(json["data"])
                self.tableView.reloadData()
                println(self.users.count)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var user = users[indexPath.row]
        var cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        
        cell.textLabel!.text = user.name
        cell.detailTextLabel?.text = user.email
        
        return cell
    }


}

