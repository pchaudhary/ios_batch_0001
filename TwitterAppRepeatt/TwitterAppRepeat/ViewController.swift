//
//  ViewController.swift
//  TwitterAppRepeat
//
//  Created by Cerebro on 20/05/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tweetTableView: UITableView!
    var tweetsArray:[[String]] = []
    var tweetDateArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTweets()
        
        tweetTableView.dataSource = self
        tweetTableView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        tweetTableView.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var tweets = tweetsArray[section]
        return tweets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        var modulus = (indexPath.row/3)%2
        
        if modulus < 1 {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "three_first")
        } else {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "three_second")
        }
        
//        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "all_cells_are_same")
        
        var tweets = tweetsArray[indexPath.section]
        
        cell.textLabel!.text = tweets[indexPath.row]
        cell.detailTextLabel!.text = "row \(indexPath.row)"
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tweetsArray.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tweetDateArray[section]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("row \(indexPath.row) of section \(indexPath.section) was selected")
    }
    
    func loadTweets() {
        
        var twitter = STTwitterAPI(OAuthConsumerKey: "TfF4vgPS5mDzA9h0iNlNZw", consumerSecret: "kjCIFPqGQVd6Rh0BQtmVLgyFbGJADI5wLaynGk7W90U", oauthToken: "379024727-l47rgTMuo1lXtEH4MC0YyA2VNbgk1nPAvYOpyd3I", oauthTokenSecret: "o1q514ANLXjPBsHqfS7iqVQsHw0VnCM8DRIIiEVE0YrhP")
        
        let errorBlock: (NSError) -> Void = {error in
            println(error.description)
        }
        
        let completionBlock: (String!) -> Void = {message in
            println(message)
        }
        
        twitter.verifyCredentialsWithSuccessBlock(completionBlock, errorBlock: errorBlock)
        
        //code to group tweets according to day
        
        tweetsArray = [
            [
                "this is my latest tweet 0",
                "this is my latest tweet 1",
                "this is my latest tweet 2",
                "this is my latest tweet 3",
                "this is my latest tweet 4"
            ],
            [
                "Yet another tweet",
                "This is my first tweet",
                "Teaching iOS to some students",
                "It was an Awesome day!!"
            ],
            [
                "this is my twitter from some other day",
                "another tweet from same day"
            ],
            [
                "Hi there! I am using WhatsApp",
                "tweet 0",
                "tweet 1",
                "tweet 2",
                "tweet 3"
            ]
        ]
        
        tweetDateArray = [
            "Today",
            "20 may, 2015",
            "14 may, 2015",
            "01/05/2015"
        ]
    }
}

