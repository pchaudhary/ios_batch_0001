//
//  ViewController.swift
//  TimeConverter
//
//  Created by Cerebro on 29/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate {

    @IBOutlet weak var countryPicker: UIPickerView!
    
    let countries = ["India", "US", "Pakistan", "UK", "KarKarDuma", "Srilanka", "Bhutan", "Australia", "New Zealand"]
    
    let population = [10, 20, 30, 40, 35, 90]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryPicker.dataSource = self
        countryPicker.delegate = self
        
        let middleIndex = countries.count/2
        
        countryPicker.selectRow(middleIndex, inComponent: 0, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        return countries[row];
    }
    
    func myFilter(element:Int) -> Bool {
        return element > 30
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        println("Alert view has been canceled by pressing \(buttonIndex)")
    }
    
    func alertViewCancel(alertView: UIAlertView) {
        println("Alert view has been canceled")
    }
    
    @IBAction func searchCountry(sender: UITextField) {
        
        println(sender.text)
        
        println(population.filter(myFilter))
        
        let alert = UIAlertView(title: "Calulated Time", message: "US time 10:30 pm is India time 8:00 AM", delegate: nil, cancelButtonTitle: "Thanks")
        
        alert.show()
        
        population.capacity
    }
}

