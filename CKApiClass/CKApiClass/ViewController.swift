//
//  ViewController.swift
//  CKApiClass
//
//  Created by Cerebro on 24/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewController: UIViewController, UITableViewDataSource {
    
    var projects = [Project]()

    @IBOutlet weak var tableView: UITableView!
    
    let token = NSUserDefaults.standardUserDefaults().objectForKey("login_token") as? String;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        if(token == nil) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("login_controller") as! CKLoginViewController
            
            showViewController(vc, sender: self)
        } else {
            Alamofire.request(.GET, "http://api.invidz.com/projects", parameters: ["token": token!]).responseJSON { (request, response, data, error) in
                
                if error != nil {
                    println(error)
                } else {
                    let projectsData = JSON(data!)
                    
                    for (key, value) in projectsData["data"] {
                        self.projects.append(Project(json: value))
                    }
                    
                    self.tableView.reloadData()
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell();
        
        cell.textLabel!.text = projects[indexPath.row].title
        
        return cell
    }

}

