//
//  ViewController.swift
//  APIDemo
//
//  Created by Cerebro on 22/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController, ListsServiceCaller {
    
    @IBOutlet weak var emailField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonTapped(sender: UIButton) {
        ListsService(caller:self).fetchLists()
        println("i have moved on")
    }
    
    func listServiceDidLoadData(data: JSON) {
        println("I have been called with data")
        println(data)
    }
    
}

