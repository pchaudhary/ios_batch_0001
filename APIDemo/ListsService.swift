//
//  ListsService.swift
//  APIDemo
//
//  Created by Cerebro on 28/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SWXMLHash

class ListsService {
    
    var myCaller:ListsServiceCaller
    
    init(caller:ListsServiceCaller) {
        self.myCaller = caller
    }
    
    func fetchLists() {
        
        Alamofire.request(.GET, "https://api.flickr.com/services/rest/", parameters: [
            "api_key": "b5fc75325e1e0a42a42a78d9f20428c6",
            "method": "flickr.photos.getRecent"
            ]).response { (req, res, data, error) in
                if(error != nil) {
                    NSLog("Error: \(error)")
                    println(req)
                    println(res)
                    
                }
                else {
                    var xml = SWXMLHash.parse(data!)
                    println(xml["rsp"]["photos"]["photo"][0].element?.attributes["id"])
                    println("Hello world")
                }
        }
        
        println("fetch lists is returning")
    }
}


protocol ListsServiceCaller {
    func listServiceDidLoadData(data:JSON)
}