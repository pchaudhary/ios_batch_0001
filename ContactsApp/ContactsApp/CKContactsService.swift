//
//  CKContactsService.swift
//  ContactsApp
//
//  Created by Cerebro on 12/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import Foundation

class CKContactsService {
    
    func contacts() -> [CKContact] {
        
        var contacts = [CKContact]()
        
        var contact:CKContact;
        
        for (var i = 0; i < 10; i++) {
            contact = CKContact(userId: i, name: "User \(i)", phoneNumber: "999999999\(i)", email: "email\(i)@example.com")
            
            contacts.append(contact)
        }
        
        return contacts
    }
}
