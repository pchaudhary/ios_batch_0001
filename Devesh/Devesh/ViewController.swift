//
//  ViewController.swift
//  Devesh
//
//  Created by Cerebro on 26/08/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let items = ["amar", "Akbar", "Sumit", "Mohit", "anthony", "Devesh", "Badal"]
    
    var groupedItems:[[String]] = []
    let alphabets:[Character] = ["A","B","C","D"]
    var alphabetsWithItems:[Character] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        var filteredItems:[String]
        
        for alphabet in alphabets {
            filteredItems = items.filter({ (item : String) -> Bool in
                let upperCaseItem = item.uppercaseString
                return upperCaseItem[upperCaseItem.startIndex] == alphabet
            })
            if filteredItems.count > 0 {
                groupedItems.append(filteredItems)
                alphabetsWithItems.append(alphabet)
            }
        }
        
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return alphabetsWithItems.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupedItems[section].count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return String(alphabetsWithItems[section])
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("any_cell") as! UITableViewCell
        cell.textLabel!.text = groupedItems[indexPath.section][indexPath.row]
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

