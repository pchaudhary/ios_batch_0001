//
//  User.swift
//  InvidzApi
//
//  Created by Cerebro on 23/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    var identifier:Int
    var name:String
    var email:String
    
    init(identifier:Int, name:String, email:String) {
        self.identifier = identifier
        self.name = name
        self.email = email
    }
    
    static func usersFromJSON(userJson:JSON) -> [User] {
        var users = [User]()
        var identifier:Int?
        var name:String?
        var email:String?
        
        for (key,user) in userJson {
            println(user)
            
            if let userId = user["id"].int{
                identifier = userId
            }
            
            if let userName = user["name"].string{
                name = userName
            }
            
            if let userEmail = user["email"].string{
                email = userEmail
            }
            if(identifier != nil && name != nil && email != nil) {
                users.insert(User(identifier:identifier!, name:name!, email:email!), atIndex: users.count)
            }
        }
        
        return users
    }
}