//
//  ViewController.swift
//  map
//
//  Created by Cerebro on 26/08/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let myArray = [3, 5, 9, 15]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let arrayOfSquares = squareArray(myArray)
        println(arrayOfSquares)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func squareArray(originalArray:[Int]) -> [Int] {
        var finalArray:[Int] = []
        
        for element in originalArray {
            finalArray.append(element*element)
        }
        
        return finalArray
    }


}

