//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Cerebro on 12/09/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userTable =  NSEntityDescription.entityForName("User",
            inManagedObjectContext:
            context)
        
        let postTable = NSEntityDescription.entityForName("Post",
            inManagedObjectContext:
            context)
        
        let post = NSManagedObject(entity: postTable!,
            insertIntoManagedObjectContext:context)
        
        let user = NSManagedObject(entity: userTable!,
            insertIntoManagedObjectContext:context)
        
        user.setValue("Gaurav", forKey: "name")
        user.setValue(true, forKey: "is_paid_user")
        user.setValue(10, forKey: "age")
        
        
        let user2 = NSManagedObject(entity: userTable!,
            insertIntoManagedObjectContext:context)
        
        user2.setValue("Gaurav", forKey: "name")
        user2.setValue(true, forKey: "is_paid_user")
        user2.setValue(10, forKey: "age")
        
        
        var someUser:NSManagedObject;
        
        for var i = 0; i < 10; i++ {
            someUser = NSManagedObject(entity: userTable!,
                insertIntoManagedObjectContext:context)
            
            someUser.setValue("user \(i)" , forKey: "name")
            user2.setValue(true, forKey: "is_paid_user")
            user2.setValue(i, forKey: "age")
            
        }
        
        var error: NSError?
        if !context.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let fetchRequest = NSFetchRequest(entityName:"User")

        var error: NSError?
        
        let fetchedResults =
        context.executeFetchRequest(fetchRequest,
            error: &error) as? [NSManagedObject]
        
        if let results = fetchedResults {
            println(results[0].valueForKey("name"))
            
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

