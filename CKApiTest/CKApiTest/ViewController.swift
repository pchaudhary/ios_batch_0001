//
//  ViewController.swift
//  CKApiTest
//
//  Created by Cerebro on 23/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Alamofire.request(.GET, "http://api.invidz.com/users", parameters: nil)
            .responseJSON { (request, response, data, error) in
                println(request)
                println(response)
                println(data)
                println(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

