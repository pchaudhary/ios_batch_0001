//
//  ViewController.swift
//  DoKaPahada
//
//  Created by Cerebro on 26/05/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet weak var pahadaTableView: UITableView!
    @IBOutlet weak var numberTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pahadaTableView.delegate = self
        pahadaTableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (section) {
        case 0:
            return "seedha pahada";
        case 1:
            return "ulta pahada";
        default:
            break
        }
        
        return nil
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell");
        
        if (numberTextField.text == nil || numberTextField.text == "") {
            cell.textLabel?.text = "please enter some number"
        } else {
            let number = numberTextField.text.toInt()
            
            if let unoptionNumber = number {
                var multiplier:Int
                
                switch (indexPath.section) {
                case 1:
                    multiplier = 10 - indexPath.row
                default:
                    multiplier = indexPath.row + 1
                }
                
                var result = unoptionNumber * multiplier
                cell.textLabel?.text = "\(unoptionNumber) x \(multiplier) = \(result)"
            } else {
                cell.textLabel?.text = "invalid number"
            }
        }
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0;
    }
    
    @IBAction func submit() {
        pahadaTableView.reloadData()
        numberTextField.resignFirstResponder()
    }
}

