//
//  ViewController.swift
//  TableViewDemo
//
//  Created by Cerebro on 20/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var KisKaaTextFiedld: UITextField!
    
    @IBOutlet weak var KahanTakTextField: UITextField!

    
    @IBOutlet weak var tableView: UITableView!
    
    var range = 10
    var number = 7
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func submitButtonTapped(sender: UIButton) {
        
        number = KisKaaTextFiedld.text.toInt()!
        
        range = KahanTakTextField.text.toInt()!
        
        KahanTakTextField.resignFirstResponder()
        KisKaaTextFiedld.resignFirstResponder()
        
        tableView.reloadData()
        
//        tableView.reloadData()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return range
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        var index:Int;
        
        if indexPath.section == 0 {
            index = indexPath.row + 1
        } else {
            index = range - indexPath.row
        }
        
        cell.textLabel!.text = "\(number) x \(index) = \(number * index)"
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title:String
        
        if section == 0 {
            title = "seedha pahada"
        } else {
            title = "ulta pahada"
        }
        
        return title
    }

}

