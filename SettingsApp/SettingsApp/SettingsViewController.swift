//
//  ViewController.swift
//  SettingsApp
//
//  Created by Cerebro on 11/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var viewController = segue.destinationViewController as! SettingDetailsViewController
        
        switch segue.identifier! {
            case "aeroplane_segue":
            viewController.infoLabelText = "show aeroplane details"
            case "wifi_segue":
            viewController.infoLabelText = "show wifi details"
            case "mobile_network":
            viewController.infoLabelText = "show mobile details"
            case "bluetooth":
            viewController.infoLabelText = "show bluetooth details"
        default:
            break
        }
    }


}

