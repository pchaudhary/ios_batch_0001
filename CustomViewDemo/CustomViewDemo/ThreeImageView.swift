//
//  ThreeImageView.swift
//  CustomViewDemo
//
//  Created by Cerebro on 05/08/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ThreeImageView: UIView {

    var view: UIView!
    
    @IBOutlet weak var imageViewOne: UIImageView!
    
    @IBOutlet weak var imageViewTwo: UIImageView!
    
    
    @IBOutlet weak var imageViewThree: UIImageView!
    
    
    @IBOutlet weak var textLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        let nib = UINib(nibName: "ThreeImageView", bundle: nil)
        view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        view.frame = self.bounds
        
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        addSubview(view)
    }

}
