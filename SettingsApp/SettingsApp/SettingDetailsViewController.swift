//
//  SettingDetailsViewController.swift
//  SettingsApp
//
//  Created by Cerebro on 11/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class SettingDetailsViewController: UIViewController {

    @IBOutlet weak var infoLabell: UILabel!
    
    var infoLabelText:String = "random_text"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        infoLabell.text = infoLabelText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
