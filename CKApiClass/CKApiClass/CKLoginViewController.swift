//
//  CKLoginViewController.swift
//  CKApiClass
//
//  Created by Cerebro on 24/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CKLoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login() {
        let email = emailField.text
        let password = passwordField.text
        
        if !email.isEmpty && !password.isEmpty {
            Alamofire.request(.GET, "http://api.invidz.com/auth/login", parameters: ["email": email, "password":password]).responseJSON { (request, response, data, error) in
                if(error != nil) {
                    println(error)
                } else {
                    let userInfo = JSON(data!)
                    var userDefaults = NSUserDefaults.standardUserDefaults()
                    
                    if let name = userInfo["user"]["name"].string {
                        println(name)
                        userDefaults.setObject(name, forKey: "name")
                    }
                    
                    if let email = userInfo["user"]["email"].string {
                        println(email)
                        userDefaults.setObject(email, forKey: "email")
                    }
                    
                    if let token = userInfo["token"].string {
                        println(token)
                        userDefaults.setObject(token, forKey: "login_token")
                        
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                }
            }
        }
    }
}
