//
//  ViewController.swift
//  ColorChangingSquare
//
//  Created by Cerebro on 17/07/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func rightSwipe(sender: UISwipeGestureRecognizer) {
        showSquareScreen(true)
        
    }
    
    @IBAction func leftSwipe(sender: UISwipeGestureRecognizer) {
        showSquareScreen(false)
    }
    
    func showSquareScreen(red:Bool) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var vc = storyboard.instantiateViewControllerWithIdentifier("second_screen") as! SecondScreenViewController
        
        vc.isRed = redr
        
        self.showViewController(vc, sender: self)
    }
    

}

