//
//  UIThreeImageView.swift
//  CustomViews
//
//  Created by Cerebro on 15/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class CKThreeImageView: UIView {

    var view: UIView!
    
    @IBOutlet weak var firstImageView: UIImageView!
    
    @IBOutlet weak var secondImageView: UIImageView!
    
    @IBOutlet weak var thirdImageView: UIImageView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "OverlappingImages", bundle: bundle)
        view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        view.frame = bounds
        
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        addSubview(view)
    }

}
