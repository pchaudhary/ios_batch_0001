//
//  ViewController.swift
//  pahadaApp
//
//  Created by Arpan Chaudhary on 10/06/15.
//  Copyright (c) 2015 Arpan Chaudhary. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var pahadaTableView: UITableView!
    
    @IBOutlet weak var numberTextField: UITextField!
    
    @IBOutlet weak var rangeTextField: UITextField!
    
    var kisKaPahada = 2
    var kahanTak = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        pahadaTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return kahanTak
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        var index = indexPath.row + 1
        var result = kisKaPahada * index
        cell.textLabel!.text = "\(kisKaPahada) x \(index) = \(result)"
        
        return cell
    }

    @IBAction func submitButtonTapped() {
        
        var showPopup = false
        
        if rangeTextField.text == nil {
            kahanTak = 10
        } else {
            if rangeTextField.text!.toInt() == nil {
                showPopup = true
            } else {
                kahanTak = rangeTextField.text!.toInt()!
            }
        }
        
        if numberTextField.text == nil {
            kisKaPahada = 2
        } else {
            if numberTextField.text!.toInt() == nil {
                showPopup = true
            } else {
                kisKaPahada = numberTextField.text!.toInt()!
            }
        }
        
        if(showPopup) {
            var alert = UIAlertView(title: "Don't try to be over smart!", message: "Please enter only integer values here", delegate: nil, cancelButtonTitle: "ok")
            
            alert.show()
        }
        
        pahadaTableView.reloadData()
        
        numberTextField.resignFirstResponder()
        rangeTextField.resignFirstResponder()
    }

}

