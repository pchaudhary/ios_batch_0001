//
//  ViewController.swift
//  DesignableView
//
//  Created by Cerebro on 18/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var firstOverlappingImageView: OverlappingImages!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        firstOverlappingImageView.imageOne = UIImage(named: "quotes");
        firstOverlappingImageView.imageTwo = UIImage(named: "great_people");
        firstOverlappingImageView.imageThree = UIImage(named: "cloud");
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

