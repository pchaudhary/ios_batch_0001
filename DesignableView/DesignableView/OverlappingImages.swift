//
//  OverlappingImages.swift
//  DesignableView
//
//  Created by Cerebro on 18/06/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class OverlappingImages: UIView {
    
    var view: UIView!
    
    @IBOutlet var imageViewOne: UIImageView!
    
    @IBOutlet weak var imageViewTwo: UIImageView!
    
    
    @IBOutlet weak var imageViewThree: UIImageView!
    
    @IBInspectable var imageOne: UIImage? {
        get {
            return imageViewOne.image
        }
        set(image) {
            imageViewOne.image = image
        }
    }
    
    @IBInspectable var imageTwo: UIImage? {
        get {
            return imageViewTwo.image
        }
        set(image) {
            imageViewTwo.image = image
        }
    }
    
    @IBInspectable var imageThree: UIImage? {
        get {
            return imageViewThree.image
        }
        set(image) {
            imageViewThree.image = image
        }
    }
    
    @IBInspectable var title:String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    
    
    func setup() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "OverlappingImages", bundle: bundle)
        view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        view.frame = bounds
        
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        addSubview(view)
    }
}
