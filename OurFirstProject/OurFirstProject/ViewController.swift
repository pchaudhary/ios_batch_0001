//
//  ViewController.swift
//  OurFirstProject
//
//  Created by Cerebro on 18/09/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonTapped(sender: UIButton) {
        
        sender.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
        
        myLabel.textColor = UIColor.redColor()
        
        println(userNameTextField.text)
        println(passwordTextField.text)
        
        println("button pressed")
    }

    @IBAction func anotherFunciton() {
    }
}

